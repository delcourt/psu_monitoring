from dummy_db import dummy_db
import time
from HMP4040 import HMP4040
from keithley import keithley
import os

db = dummy_db(file_name = "psu.db", structure = [('date', 4), ('LV1', 2), ('LV2', 2),('LV3', 2), ('LV4', 2), ('LI1', 2), ('LI2', 2),('LI3', 2), ('LI4', 2), ('HV1',3),('HV2',3),('HI1',3),('HI2',3)])

lv_psu = HMP4040("103027") #serial number = 103027
lv_psu.allow_local()
hv_psu_bottom   = keithley("4091597") #tty="ASRL/dev/ttyUSB0::INSTR") #Serial number : "4091597"
hv_psu_croc = keithley("1368815") #tty="ASRL/dev/ttyUSB1::INSTR") #Serial number : "1368815"

while True:
    try:
        entry = {}
        entry["date"] = int(time.time())
        for i in range(1,5):
            entry[f"LV{i}"] = int(1000*float(lv_psu.get_voltage(i))+0.5)
            entry[f"LI{i}"] = int(1000*float(lv_psu.get_current())+0.5)
        
        results_croc = hv_psu_croc.get_voltage_and_current()
        results_bot = hv_psu_bottom.get_voltage_and_current()
        if results_croc:
          entry["HV1"] = int(1000*results_croc[0]+0.5)
          entry["HI1"] = abs(int(1e9*results_croc[1]+0.5))
        else:
          print('Failed to get values from 1368815, Croc, Ch2')
        if results_bot:
          entry["HV2"] = int(1000*results_bot[0]+0.5)
          entry["HI2"] = abs(int(1e9*results_bot[1]+0.5))
        else:
          print('Failed to get values from 4091597, CMS, Ch1')
        if results_croc and results_bot:
          print(entry)
          db.add(entry)
          db.write_to_file()
    except Exception as e:
        print(e)
    time.sleep(15)
    
