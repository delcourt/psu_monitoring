import pyvisa, psu_common, time, os
LOCK_FILE = '.keithley'

class keithley:

    def __init__(self, serial_number=None, tty=None):
        if tty:
            self.inst = psu_common.open_device(tty)
        else:
            self.inst = psu_common.get_device('KEITHLEY', None, serial_number)
        if self.inst == None:
            print('Fatal error, unable to connect to PSU !')

    def wait_unlock(self):
        t0 = time.time()
        while LOCK_FILE in os.listdir('.'):
            print('Waiting...')
            t1 = time.time()
            time.sleep(0.1)
            if t1 - t0 > 5:
                print('COMMAND TIMEOUT !')
                return 0

        return 1

    def unlock(self):
         pass
#        os.system(f"rm {LOCK_FILE}")

    def lock(self):
         pass
#        os.system(f"touch {LOCK_FILE}")

    def query(self, command):
        if not self.wait_unlock():
            return 0
        else:
            self.lock()
            output = self.inst.query(command).replace('\x11', '').replace('\r', '').replace('\n', '').replace('\x13', '')
            self.unlock()
            return output

    def write(self, command):
        if not self.wait_unlock():
            return 0
        self.lock()
        self.inst.write(command)
        self.unlock()

    def is_enabled(self):
        return int(self.query('*ESE?'))

    def get_voltage_and_current(self):
        try:
            if int(self.query('OUTP?')):
                self.write('form:elem curr')
                float(self.query(':meas:curr?'))
                self.write('form:elem volt')
                volt = float(self.query(':meas:volt?'))
                self.write('form:elem curr')
                curr = float(self.query(':meas:curr?'))
                time.sleep(0.001)
                self.write(':SYSTem:LOCal')
                time.sleep(0.001)
                return [
                 volt, curr]
            else:
                time.sleep(0.001)
                self.write(':SYSTem:LOCal')
                time.sleep(0.001)
                return [0, 0]
        except Exception as e:
            print(f"ERROR reading keithley voltage & current: {e}")
            return 0

    def turn_on(self):
        self.write(':OUTP:STAT 1')

    def turn_off(self):
        self.write(':OUTP:STAT 0')

    def get_v_set(self):
        return float(self.query(':source:voltage?'))

    def set_v(self, volt):
        print(f"Setting : {volt}")
        self.write(f":source:voltage {volt}")


def menu(question, options):
    print(question)
    opt_str = 'Options : ['
    for o in options:
        opt_str += f" '{o}'"

    opt_str += ' ]'
    print(opt_str)
    opt_ll = [o.lower() for o in options]
    xxx = input()
    while xxx.lower() not in opt_ll:
        print(f"'{xxx}' not in {opt_str}")
        xxx = input()

    return xxx.lower()


def get_int(question):
    print(question)
    dd = input()
    while not dd.isdigit():
        print('Please enter an int')
        print(question)
        dd = input()

    return int(dd)


if __name__ == '__main__':
    psu_name = menu('Hello ! Select a HV PSU.', ['CMS', 'Croc', 'Quit'])
    if psu_name == 'croc':
        supply = keithley('1368815')
    else:
        if psu_name == 'cms':
            supply = keithley('4091597')
        else:
            sys.exit()
    action = 'None'
    while action != 'quit':
        action = menu('What do you want to do ?', ['read', 'power on', 'power off', 'ramp', 'quit'])
        if action == 'read':
            V, I = supply.get_voltage_and_current()
            print(f"Voltage = {V}V; Current = {I * 1000000.0}µA")
            print(f"V set : {supply.get_v_set()}")
        elif action == 'power on':
            #v_set = supply.get_v_set()
            #if v_set != 0:
            #    print("NO ! v_set = {v_set} != 0V. Please first set voltage to 0 volts (through 'ramp')")
            #else:
            supply.turn_on()
            print('Done !')
            V, I = supply.get_voltage_and_current()
            print(f"Voltage = {V}V; Current = {I * 1000000.0}µA")
        elif action == 'power off':
            #v_set = supply.get_v_set()
            #if v_set != 0:
            #    print('NO ! v_set = {v_set} != 0V. Please first slowly ramp voltage to 0 volts')
            #else:
            supply.turn_off()
            print('Done !')
            V, I = supply.get_voltage_and_current()
            print(f"Voltage = {V}V; Current = {I * 1000000.0}µA")
        elif action == 'ramp':
            step = 20
            r_a = ''
            while r_a != 'done':
                v_set = supply.get_v_set()
                print(f"Current voltage set :{v_set}V; step : {step}V")
                V, I = supply.get_voltage_and_current()
                print(f"Voltage = {V}V; Current = {I * 1000000.0}µA")
                r_a = menu('RAMPING action :', ['up', 'down', 'u', 'd', 'step', 'read', 'done'])
                if r_a in ('up', 'u'):
                    v_set = supply.get_v_set()
                    if v_set + step > 800:
                        print(f"You asked to go to {v_set + step} which is > 800V. Ignoring.")
                    else:
                        supply.set_v(v_set + step)
                elif r_a in ('down', 'd'):
                    v_set = supply.get_v_set()
                    if v_set - step < 0:
                        print("Won't work...")
                    else:
                        supply.set_v(v_set - step)
                elif r_a == 'step':
                    step = get_int('Select voltage step')

