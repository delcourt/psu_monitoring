
import pyvisa
import psu_common
import time,os

LOCK_FILE = ".hmp"


class HMP4040:
    def __init__(self,serial_number = None):
        self.inst = psu_common.get_device("HAMEG",None,serial_number)
        if self.inst == None:
            print("Fatal error, unable to connect to PSU !")
    def get_id(self):
        print(self.query("*IDN?"))

    def query(self,command):
        if not self.wait_unlock():
            return 0
        self.lock()
        output = self.inst.query(command).replace("\x11","").replace("\r","").replace("\n","").replace("\x13","")
        self.unlock()
        return output

    def write(self,command):
        if not self.wait_unlock():
            return 0
        self.lock()
        self.inst.write(command)
        self.unlock()
        

    def wait_unlock(self):
        t0 = time.time()
        
        while LOCK_FILE in os.listdir("."):
            print("Waiting...")
            t1 = time.time()
            time.sleep(0.1)
            if t1-t0 > 5:
                print("COMMAND TIMEOUT !")
                return 0
        return 1

    def unlock(self):
        os.system(f"rm {LOCK_FILE}")

    def lock(self):
        os.system(f"touch {LOCK_FILE}")

    def set_channel(self,channel):
        self.write(f"INST:NSEL {channel}")
        if int(self.query("INST:NSEL?")) != channel:
            print("ERROR unable to get channel")

    def get_voltage(self, channel = 0):
        #Select channel:
        if channel != 0:
            self.set_channel(channel)
        return self.query("MEAS:VOLT?")

    def get_current(self, channel = 0):
        #Select channel:
        if channel != 0:
            self.set_channel(channel)
        return self.query("MEAS:CURR?")
        
    def allow_local(self):
        self.write("SYSTem:MIX")


    def turn_on(self):
        #TODO
        return None

    def is_enabled(self,channel = 0):
        if channel != 0:
            self.set_channel(channel)
        return int(self.query("OUTP:STAT?"))

    def turn_on(self, channel = 0):
        if channel != 0:
            self.set_channel(channel)
        self.write("OUTP:STAT 1")

    def turn_off(self, channel = 0):
        if channel != 0:
            self.set_channel(channel)
        self.write("OUTP:STAT 0")
#        self.write(f"VOLT 10.4")

    def get_v_set(self, channel = 0):

        return(float(self.query("VOLT?")))
        
    def set_v(self, volt, channel = 0):
        if channel != 0:
            self.set_channel(channel)
        self.write(f"VOLT {volt}")


def menu(question, options):
    print(question)
    opt_str = "Options : ["
    for o in options:
        opt_str+=f" '{o}'"
    opt_str+=" ]"
    print(opt_str)
    opt_ll = [o.lower() for o in options]
    xxx = input()
    while not xxx.lower() in opt_ll:
        print(f"'{xxx}' not in {opt_str}")
        xxx = input()
    return xxx.lower()

def get_float(question):
    succeeded = False
    while not succeeded:
        print(question)
        print("Please enter a float")
        dd = input()
        try:
            dd = float(dd)
        except:
            pass
    return dd



if __name__=="__main__":    

    lv_psu = HMP4040("103027")
    action = ""
    while action != "quit":
        for i in range(1,5):
            print(f"Channel {i} : Enabled : {lv_psu.is_enabled(i)}; V set = {lv_psu.get_v_set()} V mon = {lv_psu.get_voltage()} I mon ={lv_psu.get_current()}")

        action = menu("Select channel to turn on/off, or read status", ["1","2","3","4", "all", "read", "quit"])
        if action in ["1","2","3","4", "all"]:
            status = menu("Turn on or off ?",["ON", "OFF", "CANCEL"])
            
            if status == "cancel":
                continue

            if action.isdigit():
                print(int(action))
                if status == "on":
                    lv_psu.turn_on(int(action))
                elif status == "off":
                    lv_psu.turn_off(int(action))
            else:
                for i in range(1,4):
                    if status == "on":
                        lv_psu.turn_on(int(action))
                    elif status == "off":
                        lv_psu.turn_off(int(action))
                
